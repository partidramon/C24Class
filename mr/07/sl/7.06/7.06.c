/*使用二维数组保存数据*/

#include <stdio.h>

int main()
{
	int a[2][3];
	int i, j;

	for (i=0; i<2; i++)
	{
		for (j=0; j<3; j++)
		{
			printf("a[%d][%d] = ", i, j);
			scanf("%d", &a[i][j]);
		}
	}
	printf("输出二维数组\n");
	for (i=0; i<2; i++)
	{
		for (j=0; j<3; j++)
		{
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
	return 0;
}
