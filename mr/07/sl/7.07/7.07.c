/*录入3x3数组，并求和所有元素*/

#include <stdio.h>

int main()
{
	int a[3][3];
	int i, j, sum;
	printf("录入3x3数组:\n");
	for (i=0; i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			printf("a[%d][%d] = ", i, j);
			scanf("%d", &a[i][j]);
		}
	}

	printf("输出3x3数组\n");
	for (i=0; i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}

	sum=0;
	for (i=0; i<3;i++)
	{
		for (j=0;j<3;j++)
		{
			sum+=a[i][j];
		}
		printf("\n");
	}
	printf("所有元素的和为%d\n", sum);

	return 0;
}
