/*折半法排序*/
#include <stdio.h>

/*声明函数*/
void CelerityRun(int left, int right, int array[]);

int main()
{
	int i, j;
	int a[10];
	int iTemp;
	int iPos;
	printf("为数组元素赋值：\n");
	/*从键盘为数组元素赋值*/
	for (i=0; i<10; i++)
	{
		printf("a[%d]=", i);
		scanf("%d", &a[i]);				/*输入数组元素*/
	}

	/*从小到大排序*/
	CelerityRun(0, 9, a);

	/*输出数组*/
	for (i=0; i<10; i++)
	{
		printf("%d\t", a[i]);			/*输出制表位*/
		if (i == 4)						/*如果是第五个元素*/
			printf("\n");				/*输出换行*/
	}
	return 0;
}	

void CelerityRun(int left, int right, int array[])
{
	int i, j;
	int middle, iTemp;
	i = left;
	j = right;
	middle = array[(left+right)/2];		/*求中间数*/
	do
	{
		while((array[i]<middle) && (i<right))	/*从左找小于中值的数*/
			i++;
		while((array[j]>middle) && (j>left))	/*从右找大于中值的数*/
			i--;
		if (i<=j)								/*找到一对值*/
		{
			iTemp = array[i];
			array[i] = array[j];
			array[j] = iTemp;
			i++;
			j--;
		}
	}while(i<=j);			/*如果两边的下标交错，则停止(完成一次)*/

	/*递归左半边*/
	if(left<j)
		CelerityRun(left, j, array);
	/*递归右半边*/
	if(right>i)
		CelerityRun(i, right, array);
}
