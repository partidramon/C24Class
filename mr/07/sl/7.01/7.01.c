/*使用数组保存数据*/

/*保存用户输入的数据，再将数据输出*/

#include <stdio.h>

int main()
{
	int iArray[5], index, temp;
	printf("Please enter a Array:\n");

	for (index=0; index<5; index++)
	{
		scanf("%d\n", &iArray[index]);
	}

	printf("Original Array is:\n");
	for (index=0; index<5; index++)
	{
		printf("%d ", iArray[index]);
	}
	printf("\n");

	return 0;
}
