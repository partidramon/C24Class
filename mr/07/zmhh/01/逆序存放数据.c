/*
 * 1，将键盘输入的数，放入数组
 * 2，将数组中的数倒序排列
 * 3，输出并显示数组
 * */

#include <stdio.h>

int main()
{
	int a[5], i, temp;							/*定义数组即变量为基本整型*/
	printf("Enter 5 number intto array:\n ");
	for (i=0; i<5; i++)							/*接收5个数字进入数组*/
		scanf("%d", &a[i]);
	printf("The array is:\n");
	for (i=0; i<5; i++)							/*展示原来的数组*/
		printf("%d ", a[i]);
	printf("\n");
	for (i=0; i<2; i++)							/*将数组前后元素的位置互换*/
	{
		temp = a[i];
		a[i] = a[4-i];
		a[4-i] = temp;
	}
	printf("Now array is:\n");
	for (i=0; i<5; i++)							/*展示现在的数组*/
		printf("%d ", a[i]);
	return 0;
}
