/**/

#include <stdio.h>

int main()
{
	/*定义数组和变量*/
	int a[5][5] =
	{
		{1, 3, 6, 7, 28},
		{12, 63, 31, 2, 6},
		{1, 65, 36, 34, 2},
		{12, 53, 83, 2, 6},
		{12, 3, 6, 3, 6}
	};
	int i, j, sum;
	/*展示二维数组*/
	for (i=0; i<5; i++)
	{
		for (j=0; j<5; j++)
			printf("%2d ",a[i][j]);
		printf("\n");
	}
	/*求对角线之和*/
	sum=0;
	for (i=0, j=4; i<5; i++,j--)
	{
		sum=sum + a[i][i] + a[i][j];
	}
	sum-=a[3][3];
	printf("The sum of cross line is %d\n", sum);
	return 0;
}
