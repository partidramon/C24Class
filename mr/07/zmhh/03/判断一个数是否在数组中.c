/*
 * 1，定义一个数组
 * 2，为祖国数组初始化
 * 3，接收一个数
 * 4，判断这个数是否在数组之中
 * */

#include <stdio.h>

int main()
{
	int i, num, k;								/*声明变量*/
	int a[10]={10, 23, 21, 43, 43, 
		53, 64, 23, 90, 78};					/*初始化数组*/
	k=11;
	printf("Please input the number witch you want to find:\n");
	scanf("%d",&num);
	for (i=0; i<10; i++)
	{
		if (num==a[i])
			k=i;
	}
	if(k!=11)
	{
		printf("The member you find is in the array!\n");
	}
	else
	{
		printf("Have not find the number!\n");
	}
	return 0;
}
