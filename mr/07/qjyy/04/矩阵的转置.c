/*
矩阵的的转置
将一个二维数组的行和列元素互换，存到另一个二维数组中
*/

#include <stdio.h>
#define MAX 101

int main()
{
    //定义变量
    int a[MAX][MAX], b[MAX][MAX], i, j, row, col;

    //确认二维数组的行数和列数，并接收数据项
    printf("please input the number of rows(<100):\n");
    scanf("%d", &row);
    printf("please input the number of columns(<100):\n");
    scanf("%d", &col);
    printf("Please input %d elements:\n", col*row);
    for (i=0; i<col; i++)
        for (j=0; j<row; j++)
            scanf("%d", &a[i][j]);

    //展示二维数组a
    printf("array a:\n");
    for (i=0; i<col; i++)
    {
        for (j=0; j<row; j++)
        {
            printf("%5d", a[i][j]);
        }
        printf("\n");
    }

    //矩阵转置
    for (i=0; i<col; i++)
        for (j=0; j<row; j++)
            b[j][i] = a[i][j];

    //展示二维数组b
    printf("array b:\n");
    for (i=0; i<row; i++)
    {
        for (j=0; j<col; j++)
        {
            printf("%5d", b[i][j]);
        }
        printf("\n");
    }
    return 0;
}
