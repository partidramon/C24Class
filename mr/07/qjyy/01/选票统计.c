/*
 * 选票统计
 *
 * 3候选人
 * 输入参加选举的人数以及每个人选举的内容
 * 输出3个候选人最终的得票数和无效选票数
 *
 */
 
#include <stdio.h>

int main()
{
	int i, v0=0, v1=0, v2=0, v3=0, n, a[50];
	printf("Please input the number of electorte:\n");					/*输入参加选举的人数*/
	scanf("%d", &n);
	printf("Please input 1 or 2 or 3\n");								/*输入每个人所选举的对象*/
	for (i=0; i<n; i++)
		scanf("%d", &a[i]);
	for (i=0; i<n; i++)													/*统计每个候选人的得票数*/
	{
		if (a[i]==1)
			v1++;
		else if (a[i]==2)
			v2++;
		else if (a[i]==3)
			v3++;
		else 
			v0++;
	}
	printf("The Result:\n");
	printf("candldate1:%d\n candldate2:%d\n candldate3:%d\n nonuser:%d\n",
			v1, v2, v3, v0);
	return 0;
}
