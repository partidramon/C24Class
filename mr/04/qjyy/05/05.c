/* 根据输入判断能否组成三角形 */

#include <stdio.h>
#include <math.h>

int main()
{
	float a,b,c;								/* 定义变量，储存3个边的长度 */
	scanf("%f,%f,%f",&a,&b,&c);					/* 输入3条边 */
	if (a + b > c && a + c > b && b + c > a)	/* 判断两边之和是否大于第三边 */
	{
		printf("can compose triangle.\n");		/* 可以组成三角形 */
	}
	else
		printf("can not compose triangle.\n");	/* 不能组成三角形 */
	return 0;
}





