/* 用*号输出图案 */

#include <stdio.h>

int main()
{
	printf("*   * ***\n");
	printf("** ** *  *\n");
	printf("* * * ***\n");
	printf("*   * *  * \n");
	printf("*   * *   *\n");
	return 0;
}
