/* 输出3x3的矩阵 */

#include <stdio.h>

int main()
{
	int a[4][4],i,j;
	printf("Please input numbers:\n");
	for (i=i;i<=3;i++)							/* 从键盘输入9个数到二维数组 */
		for (j=1;j<=3;j++)
			scanf("%d",&a[i][j]);
	printf("\nThe 3*3 matrix is:\n");
	for (i=1;i<=3;i++)
	{
		for (j=1;j<=3;j++)
			printf("%4d",a[i][j]);				/* 将3x3矩阵输出 */
		printf("\n");							/* 每输出一行进行换行 */
	}
	return 0;
}
