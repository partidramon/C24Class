/* 计算圆的面积 */

#include <stdio.h>

int main()
{
	float Pie=3.14f;					 	/* 定义圆周率 */

	float fArea;							/* 定义变量，表示圆的面积 */
	float fRadius;							/* 定义变量，表示圆的半径 */

	puts("Enter the radius: ");				/* 输出提示信息 */
	scanf("%f",&fRadius);					/* 输入圆的半径 */
	fArea=fRadius*fRadius*Pie;				/* 计算圆的面积 */
	printf("The Area is :%.2f\n",fArea);	/* 输出计算的结果 */
	return 0;
}
