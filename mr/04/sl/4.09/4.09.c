/* 使用附加格式说明scanf函数的格式输入 */

#include <stdio.h>

int main()
{
	long iLong;													/* 长整型变量 */
	short iShort;												/* 短整型变量 */
	int iNumber1=1;												/* 整型变量，赋值为1 */
	int iNumber2=2;												/* 整型变量，赋值为2 */
	char cChar[10];												/* 定义字符数组变量 */

	printf("Enter the long integer:\n");						/* 输出信息提示 */
	scanf("%ld",&iLong);										/* 输入长整型数据 */
	
	printf("Enter the whort integer:\n");						/* 输出信息提示 */
	scanf("%hd",&iShort);										/* 输入短整型手数据 */
	
	printf("Eneter the two number:\n");							/* 输出信息提示 */
	scanf("%d*%d",&iNumber1,&iNumber2);							/* 输入整型数据 */

	printf("Enter the stribg but only show 3 characters:\n");	/* 输出信息提示 */
	scanf("%3s",cChar);											/* 输入字符串 */

	printf("the long integer is %ld\n",iLong);					/* 显示长整型值 */
	printf("the short integer is %hd\n",iShort);				/* 显示短整型值 */
	printf("the Number1 is %d\n",iNumber1);						/* 显示整型iNumber1的值 */
	printf("the Number2 is %d\n",iNumber2);						/* 显示整型iNumber2的值 */
	printf("the three characters are %s\n",cChar);				/* 显示字符串 */
	return 0;
}
