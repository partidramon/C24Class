/* 使用格式输出函数printf */

#include <stdio.h>

int main()
{
	int iInt=10;									/* 定义整型变量 */
	char cChar='a';									/* 定义字符变量 */
	float fFloat=12.34f;							/* 定义浮点变量 */

	printf("The int is %d\n",iInt);					/* 使用printf函数输出整型 */
	printf("The char is %c\n",cChar);				/* 输出字符型 */
	printf("The float is %f\n",fFloat);				/* 输出浮点型 */
	printf("The string is %s\n" ,"I Love you.");	/* 输出字符串 */
	return 0;
}
