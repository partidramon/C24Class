/* 输出两个数的最大公约数 */

#include <stdio.h>

int main()
{
	int a,b,c,t;								/* 定义变量为基本整型 */
	printf("输入两个数:\n");
	scanf("%d%d",&a,&b);						/* 从键盘中个输入两个数 */
	if (a < b)									/* 当a<b时，两值互换 */
	{
		t = a;
		a = b;
		b = t;
	}
	c = a % b;									/* a对b取余数赋给c */
	while (c != 0)								/* 当c不为0时执行循环体语句 */
	{
		a = b;									/* 将b赋给a */
		b = c;									/* 将c赋给b */
		c = a % b;								/* 继续取余并赋给c */
	}
	printf("这两个数的最大公约数为：%d\n",b);	/* 输出最大公约数 */
	return 0;
}
