/* 输出乘法口诀表 */

#include <stdio.h>

int main()
{
	int i,j;								/* 定义i、j两个变量为基本整型 */
	for (i=1;i<=9;i++)						/* for循环中i为乘法口诀表中的行数 */
	{
		for (j=1;j<=i;j++)					/* 乘法口诀表中的另一个因数，取值范围受另一个因数的影响 */
			printf("%d*%d=%d ",i,j,i*j);		/* 输出i、j以及i*j的值 */
		printf("\n");						/* 打印完每行后换行 */
	}
	return 0;
}
