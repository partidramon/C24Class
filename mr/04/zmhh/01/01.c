/* 使用字符函数输入/输出字符 */

#include <stdio.h>
#include "conio.h" // liunx 没有getch()和getche()

int main()
{
	char c1,c2,c3;									/* 定义字符变量 */
	printf("输入一个字符，使用getche函数接收\n");	/* 提示用户输入一个字符 */
	c1=getche();									/* 使用getche()函数接收 */
	printf("\n");									/* 输出一行空行 */

	printf("输入一个字符，使用getch函数接收");		/* 提示用户输入一个字符 */
	c2=getch();										/* 使用getch()函数接收 */
	printf("\n");									/* 输出一行空行 */

	printf("输入一个字符，使用getchar函数接收");	/* 提示用户输入一个字符 */
	c3=getchar();									/* 使用getchar()函数接收 */

	/* 将这些字符输出 */
	putchar(c1);
	putchar(c2);
	putchar(c3);
	printf("\n");
	return 0;
}
