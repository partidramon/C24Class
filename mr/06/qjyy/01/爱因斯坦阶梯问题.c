/*
 * 有一条阶梯。
 * 如果每步跨2阶，则剩余1阶；
 * 如果每步跨3阶，则剩余2阶；
 * 如果每步跨5阶，则剩余4阶；
 * 如果每步跨6阶，则剩余5阶；
 * 如果每步跨7阶，则刚好走完
 *
 * 阶梯至少有多少阶？；
 * */

#include <stdio.h>

int main()
{
	int i;
	for (i=100; i<1000; i++)
		if (i%2==1 && i%3==2 && i%5==4 && i%6==5 && i%7==0)
			printf("the number of the stairs is %d\n", i);
	return 0;
}
