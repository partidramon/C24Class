/*
 *
 * 存5年的年利率为2.5%，某人存了一笔钱，预计今后5年中，每年年底取出1000元，到第5年刚好取完，
 * 问最开始他存了多少钱？
 * */

#include <stdio.h>

int main()
{
	int i;
	float total=0;
	for (i=0; i<5; i++)
		total=(total+1000)/(1+0.025);
	printf("must save %5.2f at first.\n", total);	
	return 0;
}
