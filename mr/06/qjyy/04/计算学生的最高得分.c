/*
 * 录入20个学生的某科目分数，找出这20个分数中的最大值
 * */

#include <stdio.h>
#define N 20

int main()
{
	int i;
	int score, max;
	printf("eneter score of course number:\n");
	scanf("%d", &max);
	for (i=2; i<=N; i++)
	{
		scanf("%d", &score);
		if (score > max)
			max = score;
	}
	printf("the max of course is %d\n", max);
	return 0;
}
