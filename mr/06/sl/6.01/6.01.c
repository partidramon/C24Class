/* 计算1加到100的值 */

#include <stdio.h>

int main()
{
	int iSum=0;						/* 定义变量 */
	int iNumber=1;

	while (iNumber<=100)
	{
		iSum = iSum + iNumber;
		iNumber++;
	}
	printf("The result is %d\n",iSum);
	return 0;
}
