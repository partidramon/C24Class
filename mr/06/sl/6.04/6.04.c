/* 使用for循环显示随机数 */

#include <stdio.h>

int main()
{
	int counter;											/* 定义变量 */
	/* 使用for语句，为变量赋值，执行循环 */
	for (counter=0;counter<10;counter++)
	{
		srand(counter+1);									/* 设置随机发生数的种子 */
		printf("Random number %d is %d\n",counter,rand());	/* 产生随机发生数 */
	}
	return 0;
}
