/* 使用while语句为用户提供菜单显示 */

#include <stdio.h>

int main()
{
	int iSelect = 1;		/* 定义变量，表示菜单的选项 */

	while (iSelect==0)
	{
		/* 显示菜单内容 */
		
		printf("*******Menu*********\n");
		printf("	1. Sell\n");
		printf("	2. Buy\n");
		printf("	3. Show Prodeuct\n");
		printf("	0. Quit\n");
		
		scanf("%d",&iSelect);
		switch (iSelect)
		{
			case 1:
				printf("You are buying seomething into store.\n");
				break;
			case 2:
				printf("you are selling to consumer\n");
				break;
			case 3:
				printf("checking the store\n");
				break;
			case 0:
				printf("the Program is out\n");
				break;
			default:
				printf("You put a wrong selection\n");
				break;
		}
	}
	return 0;
}
