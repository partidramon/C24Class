/* 使用do...while语句计算1到100之间所有数字的累加结果 */

#include <stdio.h>

int main()
{
	int iNumber;						/* 定义变量，表示数字 */
	int iSum=0;							/* 表示计算的总和 */

	do
	{
		iSum=iSum+iNumber;				/* 计算累加的综合 */
		iNumber++;						/* 进行自身加1 */
	}
	while (iNumber<=100);				/* 检验条件 */

	printf("the result is %d\n",iSum);	/* 输出计算结果 */
	return 0;
}
