//特殊等式
//对于等式xyz+yzz=532，编程求x、y、z的值。（xyz和yzz分别表示一个3位数）
//
//
//思路
//对x、y、z分别穷举

#include <stdio.h>

int main()
{
	int x, y, z, i;
	for (x=1;x<10;x++)					//穷举x
		for (y=1;y<10;y++)				//穷举y
			for (z=0;z<10;z++)			//穷举z
			{
				i = 100*x + 10*y + z + 100*y + 10*z + z; //求和
				if (i == 532)
					printf("X=%d, y=%d, z=%d\n", x, y, z);
			}
	return 0;
}

