/* 
 * 素数，是大于1的整数。素数不能被1和它本身整除。
 * 
 *
 * 检测一个数是不是素数，用i被2到根号i除。如果能整除，则i不是素数，结束循环
 * */

#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main()
{
	int i, j, n=0;
	system("cls");
	printf("10-100之间的素数:\n");
	for (i=10; i<=100; i++)
	{
		for (j=2; j<=sqrt(i); j++)
		{
			if (i % j ==0)
				break;
			else
			{
				if (j > sqrt(i)-1)
				{
					printf("%d,", i);
					n++;
					if (n%5==0)
						printf("\n");
				}
				else
					continue;
			}
		}
	}
	return 0;
}
