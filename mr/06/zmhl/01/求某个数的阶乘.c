/*求某个数的阶乘*/

#include <stdio.h>

int main()
{
	int i=2,n;									/*定义变量i、为基本整型并为i赋值为2*/
	float fac=1;								/*定义fac为单精度型并赋初始值1*/
	printf("please input an interger>=0.\n");
	scanf("%d",&n);								/*使用scanf函数获取n的值*/
	if(n==0||n==1)								/*当n为0或1时输出阶乘为1*/
	{
		printf("factorial is 1.\n");
		return 0;
	}
	while(i<=n)									/*当满足输入的数值大于1时执行循环语句*/
	{
		fac=fac*i;								/*实现求阶乘的过程*/
		i++;									/*变量i自加*/
	}
	printf("factorial of %d is %.2f\n",n,fac);	/*输出n和fac的最终的值*/
	return 0;
}
