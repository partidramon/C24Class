/* 字符常量的输出 */

#include <stdio.h>

int main()
{
	putchar('H');		/* 输出字符常量 */
	putchar('e');		/* 输出字符常量 */
	putchar('l');		/* 输出字符常量 */
	putchar('l');		/* 输出字符常量 */
	putchar('o');		/* 输出字符常量 */
	putchar('\n');		/* 进行换行 */

	return 0;
}
