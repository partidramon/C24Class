/* 使用双精度类型变量 */

#include <stdio.h>

int main()
{
	double dDoubleStyle;					/* 定义一个双精度类型变量 */
	dDoubleStyle=61.456;					/* 为变灵赋值 */
	printf("%f\n", dDoubleStyle);			/* 显示变量值 */
	return 0;								/* 程序结束 */
}

