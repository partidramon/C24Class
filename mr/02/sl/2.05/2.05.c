/* 使用单精度类型变量 */

#include <stdio.h>

int main()
{
	float fFloatStyle;						/* 定义单精度类型变量 */
	fFloatStyle=1.23f;						/* 为变量赋值 */
	printf("%f\n", fFloatStyle);			/* 输出变量的值 */
	return 0;								/* 程序结束 */
}

