/* 有符号基本类型 */

#include <stdio.h>

int main()
{
	signed int iNumber;			/* 定义有符号基本整型变量 */
	iNumber=10;					/* 为变量进行赋值 */
	printf("%d\n", iNumber);	/* 显示整型变量 */
	return 0;					/* 程序结束 */
}

