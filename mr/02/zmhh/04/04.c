/* 实型变量的使用 */

#include <stdio.h>

int main()
{
	float a;									/* 定义单精度变量 */
	double b;									/* 定义双精度变量 */
	long double c;								/* 定义长双精度变量 */

	a=123.456;									/* 给变量a赋值 */
	b=123.456;									/* 给变量b赋值 */
	c=123.456;									/* 给变量c赋值 */
	printf("a=%f\nb=%lf\nc=%Lf\n",a,b,c);		/* 输出字符变量 */
	return 0;
}

