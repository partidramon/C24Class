/* 字符变量的使用 */

#include <stdio.h>

int main()
{
	char a,b,c,d;					/* 声明变量 */
	a='m';							/* 给变量a赋值 */
	b='r';							/* 给变量b赋值 */
	c='j';							/* 给变量c赋值 */
	d='k';							/* 给变量k赋值 */
	printf("%c%c%c%c\n",a,b,c,d);	/* 输出字符变量 */
	return 0;
}

