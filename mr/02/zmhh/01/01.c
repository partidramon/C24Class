/* 定义正确的数据类型求圆的周长 */

#include <stdio.h>

int main()
{
	float r, girt;				/* 定义浮点型变量 */
	r=2;						/* 给变量赋值 */
	girt=2*3.14*r;				/* 计算圆周长 */
	printf("%lf\n", girt);		/* 输出圆周长 */

	return 0;
}

