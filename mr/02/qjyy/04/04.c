/* 利用"*"输出矩形 */

#include <stdio.h>

int main()
{
	char a;									/* 定义字符变量 */
	a='*';									/* 给变量赋值 */
	printf("%c%c%c\n",a,a,a);				/* 输出变量和转义字符 */
	printf("%c\40%c\n",a,a);				/* 输出变量和转义字符 */
	printf("%c%c%c\n",a,a,a);				/* 输出变量和转义字符 */
	return 0;
}
