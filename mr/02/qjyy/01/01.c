/* 十进制转二进制 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i,j,m,n;								/* 定义变量i，j，m，n */
	int a[16]=
	{
		0
	};											/* 定义数组a，元素初始值为0 */
	system("cls");								/* 清屏 */
	/* 输出双引号内普通字符 */
	printf("Please input the decimalism number(0~32767):\n");
	scanf("%d",&n);								/* 输入n的值 */
	for (m = 0; m < 15; m++)					/* for循环从0到14，最高位为符号位，本题始终为0 */
	{
		i = m % 2;								/* 取2的余数 */
		j = n / 2;								/* 取被2整除的结果 */
		n = j;									/* 将得到的商赋值被变量n */
		a[m]=i;									/*将余数存入数组a中*/
	}
	for (m = 15; m >= 0; m--)
	{
		printf("%d",a[m]);						/* for循环，将数组中的16个元素从后到前输出 */
		if (m % 4 == 0)
			printf(" ");						/* 每4个元素输出一个空格 */
	}
	printf("\n");
	return 0;
}

