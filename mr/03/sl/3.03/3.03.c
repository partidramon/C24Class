/* 显示类型转换的结果 */

#include <stdio.h>

int main()
{
	char cChar;									/* 字符型变量 */
	short int iShort;							/* 短整型变量 */
	int iInt;									/* 整型变量 */
	float fFloat=70000;							/* 单精度浮点型变量 */

	cChar=(char)fFloat;
	iShort=(short)fFloat;
	iInt=(int)fFloat;

	printf("The char is %c\n",cChar);
	printf("The short is %d\n",iShort);
	printf("The int is %d\n",iInt);
	printf("The float is %f\n",fFloat);
	return 0;
}
