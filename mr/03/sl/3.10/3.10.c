/* 使用复合赋值运算符简化赋值运算 */

#include <stdio.h>

int main()
{
	int iTotal,iValue,iDetail;										/* 声明变量 */
	iTotal=100;														/* 为变量赋值 */
	iValue=50;
	iDetail=5;

	iValue*=iDetail;
	iTotal+=iValue;
	printf("Value is %d\n",iValue);
	printf("Total is %d\n",iTotal);
	return 0;
}
