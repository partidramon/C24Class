/* 使用算术表达式计算摄氏温度 */

#include <stdio.h>

int main()
{
	int iCeisuis,iFahrenheit;										/* 声明两个变量 */
	printf("Please enter temperature:\n");							/* 显示提示信息 */
	scanf("%d",&iFahrenheit);										/* 在键盘上输入华氏温度 */
	iCeisuis=5*(iFahrenheit-32)/9;									/* 通过算术运算表达式进行计算，并将结果赋值 */
	
	printf("Temperature is :");										/* 显示提示信息 */
	printf("%d",iCeisuis);											/* 显示摄氏温度 */
	printf(" degrees Ceisius\n");									/* 显示提示信息 */
	return 0;
}

