/* 为变量赋值 */

#include <stdio.h>

int main()
{
	int iHoursWorked=8;								/* 定义变量，为变量赋初始值，表示工作时间 */
	int iHourlyRate;								/* 声明变量，表示一个小时的薪水 */
	int iGrossPay;									/* 声明变量，表示得到的工资 */

	iHourlyRate=13;									/* 为变量赋值 */
	iGrossPay=iHoursWorked*iHourlyRate;				/* 将表达式的结果赋值给变量 */

	printf("The HoursWorked is %d\n",iHoursWorked);	/* 显示工作时间变量 */
	printf("The HourlyRate is %d\n",iHourlyRate);	/* 显示一小时的薪水 */
	printf("The GrossPay is %d\n",iGrossPay);		/* 显示得到的工资 */
	return 0;
}
