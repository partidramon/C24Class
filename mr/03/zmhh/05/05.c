/* 关系表达式进行算术运算 */

#include <stdio.h>

int main()
{
	int i,j,k;						/* 定义变量 */
	i=j=k=5;						/* 给变量赋值 */
	i=j==k;							/* 比较j和k的值是否相等，然后将结果赋给变量i */
	printf("i=%d,j=%d,k=%d\n",i,j,k);	/* 输出变量的值 */
	i=(j+(k++>3));						/* 首先计算k++>3的 */
	printf("i=%d,j=%d,k=%d\n",i,j,k);	/* 输出变量的值 */
	return 0;
}
