/* 逗号运算符的应用 */

#include <stdio.h>

int main()
{
	int a,b,c,d;								/* 定义变量 */
	a=10,b=10,c=10;								/* 给变量赋值 */
	d=(c++,c+10,100-c);							/* 逗号表达式 */
	printf("a=%d,b=%d,c=%d,d=%d\n",a,b,c,d);	/* 输出便变量的值 */
	c=(d=a+b),(b+d);							/* 逗号表达式 */
	printf("a=%d,b=%d,c=%d,d=%d\n",a,b,c,d);	/* 输出便变量的值 */
	return 0;
}

