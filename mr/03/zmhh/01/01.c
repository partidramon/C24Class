/* 使用基本算术运算符  */

#include <stdio.h>

int main()
{
	int a=8,b=-3;								/* 定义两个变量 */
	printf("a%%b = %d\n",a%b);					/* 输出结果 */
	printf("a/b*b = %d\n",a/b*b);				/* 输出结果 */	
	printf("-a%%b = %d\n",-a%b);				/* 输出结果 */
	return 0;
}

