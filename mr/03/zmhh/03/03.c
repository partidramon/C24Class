/* 符合赋值运算符的使用 */

#include <stdio.h>

int main()
{
	int a,b,c;										/* 定义整型变量 */
	a=10;											/* 给变量赋值 */
	b=3;											/* 给变量赋值 */
	printf("a-b+++1=%d\n",a-b+++1);					/* 输出表达式的值 */
	a=b=c=5;										/* 给变量赋值 */
	a*=b=c-2;										/* 计算表达式的值 */
	printf("a=%d,b=%d,c=%d\n",a,b,c);				/* 输出各个变量的值 */
	return 0;
}
