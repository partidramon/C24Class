/* 赋值表达式类型的转换 */

#include <stdio.h>

int main()
{
	int a,b=105;									/* 定义变量，并对b初始化 */
	char c,d='a';									/* 定义变量，并对d初始化 */
	float x,y=2.22;									/* 定义变量，并对y初始化 */
	a=y;											/* 赋值 */
	c=b;											/* 赋值 */
	x=d;											/* 赋值 */
	printf("a=%d,c=%c,x=%5.2f\n",a,c,x);			/* 输出结果 */
	return 0;
}
