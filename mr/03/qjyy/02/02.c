/* 计算学生平均身高 */

#include <stdio.h>

int main()
{
	float a1=0,a2=0,a3=0;						/* 定义储存学生身高的变量并赋值 */
	float avg=0;								/* 定义储存你平均身高的变量并赋值 */
	printf("输入3个学生的身高：（单位：cm）\n");	/* 输出提示信息：提示用户输入三个学生的身高 */
	scanf("%f%f%f",&a1,&a2,&a3);
	avg=(a1+a2+a3)/3;
	printf("平均身高为：%f\n",avg);

	return 0;
}
