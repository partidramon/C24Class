/* 计算a+=a*=a/=a-6 */

#include <stdio.h>

int main()
{
	int a=12;				/* 定义变量并赋值 */
	a+=a*=a/=a-6;			/* 计算表达式的值 */
	printf("%d\n",a);		/* 输出计算结果 */
	return 0;
}
