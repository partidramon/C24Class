/* 计算1~10的累加和 */

#include <stdio.h>

int main()
{
	int sum;					/* 定义变量 */
	sum=1+2+3+4+5+6+7+8+9+19;	/* 计算1~10的累加和 */
	printf("%d\n",sum);			/* 输出计算结果 */
	return 0;
}

