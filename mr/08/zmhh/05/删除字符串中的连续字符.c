/*删除字符串中的连续字符*/

#include<stdio.h>
#include<string.h>

char del(char s[], int pos, int len)                /*自定义删除函数*/
{
    int i;
    for (i=pos+len-1; s[i]!='\0'; i++, pos++)     /*i初始值为指定删除部分后的第一个字符*/
        s[pos-1]=s[i];                              /*用删除部分后的字符一次从删除部分开始覆盖*/
    s[pos-1]='\0';                                  /*在重新得到的字符后加上字符串结束标志*/
    return s;
}

main()
{
    char str[50];                                   /*定义字符型数组*/
    int position;
    int length;
    printf("\nPlease input srting:");
    gets(str);                                      /*使用gets函数获得字符串*/
}
