/*将已经升序排列好的字符串a和字符串b，按照升序归并到字符串c中，并输出*/

#include<stdio.h>>
#include<string.h>

int main()
{
    char a[100], b[100], c[200], *p;
    int i=0, j=0, k=0;
    printf("please input string a:\n");         /*输入字符串1放入a数组中*/
    scanf("%s", a);
    printf("please input string b:\n");         /*输入字符串2放入b数组中*/
    scanf("%s", b);

    while (a[i]!='\0'&&b[j]!='\0')
    {
        if (a[i]<b[j])                              /*判断a中字符是否小于b中字符*/
        {
            c[k]=a[i];                              /*如果小于，将a中字符放到数组c中*/
            i++;                                    /*i自加*/
        }
        else
        {
            c[k]=b[j];                             /*如果不小于，将b中字符放到数组c中*/
            j++;                                    /*j自加*/
        }
        k++;                                        /*k自加*/
    }
    c[k]='\0';                                     /*将两个字符串合并到c中后加结束符*/
    if (a[i]=='\0')                               /*判断a中的字符是否全都复制到c中*/
        p = b + j;                                 /*p指向数组b中国为复制到c的位置*/
    else
        p = a + i;                                 /*p指向数组a中未复制到c的位置*/
    strcat(c, p);                                  /*将p指向位置开始的字符串连接到c*/
    puts(c);                                       /*将c输出*/

    return 0;
}
