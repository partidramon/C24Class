#include<stdio.h>
#include<string.h>

int main()
{
    char stuname[3][20];                            /*声明一个字符型二维数组*/
    int i;                                          /*声明一个整型变量*/
    printf("Please input 3 students names:\n");/*在屏幕上输出提示信息*/
    for (i=0; i<3; i++)                            /*循环执行3次*/
    {
        gets(stuname[i]);                           /*从键盘输入字符串，储存到数组中*/
    }
    printf("Now teh student's name in the array is :\n");
    for (i=0; i<3; i++)
    {
        printf("%s\n", stuname[i]);               /*输出数组元素*/
    }
    return 0;
}
