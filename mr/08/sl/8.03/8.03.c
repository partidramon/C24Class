/*使用两种方式输出字符串*/

#include<stdio.h>

int main()
{
    int iindex;                             /*循环控制变量*/
    char cArray[12] = "MingRi KeJi";      /*定义字数数组用于保存字符串*/

    for (iindex=0; iindex<12; iindex++)
    {
        printf("%c", cArray[iindex]);       /*逐个输出字符数组中的字符*/
    }
    printf("\n%s\n", cArray);               /*直接将字符串输出*/
    return 0;
}
