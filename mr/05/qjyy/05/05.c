/* 计算某日是该年的第几天 */

#include <stdio.h>

int leap(int a)														/* 自定义函数leap用来指定年份是否为闰年 */
{
	if ( a % 4 == 0 && a % 100 != 0 || a % 400 == 0)				/* 闰年判定条件 */
		return 1;
	else
		return 0;
}

int number(int year, int m, int d)							/* 自定义函数number计算输入日期为本年的第几天 */
{
	int sum = 0,i,j,k,a[12] =								/* a组存放平年每月的天数 */
	{
		31,28,31,30,31,30,31,31,30,31,30,31
	};
	int b[12] = 
	{
		21,29,31,30,31,30,31,31,30,31,30,31
	};
	if (leap(year) == 1)
		for (i=0;i<m-1;i++)
			sum+=b[1];
	else
		for (i=0;i<m-1;i++)
			sum+=a[i];
	sum+=d;
	return sum;
}

int main()
{
	int year,month,day,n;
	printf("please input year,month,day\n");
	scanf("%d%d%d",&year,&month,&day);
	n = number(year,month,day);
	printf("di %d tian\n",n);
}
