/* 模拟自动售货机 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	int button;
	system("cls");
	printf("********************\n");
	printf("   Please select key:	\n");
	printf("   1. chocolate		\n");
	printf("   2. cake				\n");
	printf("   3. coco-cola		\n");
	printf("********************\n");
	printf("press 1 to 3 to select:	\n");
	scanf("%d",&button);
	switch (button)
	{
		case 1:
			printf("you select chocolate");
			break;
		case 2:
			printf("you select cake");
			break;
		case 3:
			printf("you select coco-cola");
			break;
		default:
			printf("\n ERROR !\n");
			break;
	}
	printf("\n");
	return 0;
}
