/* 从小到大输出3个数 */

#include <stdio.h>

int main()
{
	int a,b,c,t;										/* 定义4个基本整型变量a、b、c、d、t */
//	clscr();/* 清屏 */
	printf("please input a,b,c:\n");					/* 输出提示信息 */
	scanf("%d%d%d",&a,&b,&c);							/* 输入任意三个数 */
	if (a>b)											/* 如果a大于b，使用中间变量t交换a和b */
	{
		t=a;
		a=b;
		b=t;
	}
	if (a>c)											/* 如果a大于c，使用中间变量t交换a和c */
	{
		t=a;
		a=c;
		c=t;
	}
	if (b>c)											/* 如果b大于c，使用中间变量t交换b和c */
	{
		t=b;
		b=c;
		c=t;
	}
	printf("the order of the number is :\n");
	printf("%d,%d,%d",a,b,c);
	return 0;
}
