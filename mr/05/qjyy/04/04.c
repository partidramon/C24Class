/* 模拟ATM机界面程序 */

#include <stdio.h>
#include <stdlib.h>

int main()
{
	char Key, CMoney;											/* 定义变量 */
	int password,password1=123,i=1,a=1000;
	while (1)
	{
		do {
			system("cls");
			printf("****************************\n");
			printf("   Please select key:		\n");
			printf("   1. password				\n");
			printf("   2. get money				\n");
			printf("   3. Return				\n");
			printf("****************************\n");
			Key = getch();
		} while (Key!='1' && Key!='2' &Key!='3');
		/* 当输入的值不是1,2,3中任意一个时，显示都循环体中的内容 */
		
		switch (Key)
		{
			case '1':
				system("cls");									/* 输入值为1时执行case 1 */
				do
				{
					i++;
					printf("   Please input password	");
					scanf("%d",&password);
					if (password1!=password)					/* 如果输入密码不正确，，执行下面的语句 */
					{
						if (i>3)								/* 如果输入3次密码均不正确将退出程序 */
						{
							printf("Wong! Press any key to exit...  ");
							getch();
							exit(0);
						}
						else 
							puts("wrong, try again");			/* 输入次数未到3次，可以继续输入 */
					}
				}
				while (password1!=password&&i<=3);
				/* 如果密码不正确且输入字数小于等于3次，执行do循环体中的语句 */
				printf("OK! Press any key to continue... ");	/* 密码正确返回出事界面开始其他操作 */
				getch();
			case '2':											/* 输入值为2时执行case2*/
				do
				{
					system("cls");
					if (password1!=password)
						/* 如果在case1中密码输入不正确将无法进行后面的操作 */
					{
						printf("Please logging in,Press any key to continue... ");
						getch();
						break;
					}
					else
					{
						printf("****************************\n");
						printf("   Please select:			\n");
						printf("   1. $100					\n");
						printf("   3. $200					\n");
						printf("   3. $300					\n");
						printf("   4. Return				\n");
						printf("****************************\n");
						CMoney = getch();
					} 
				} while (CMoney!='1' && CMoney!= '2' && CMoney != '3' && CMoney != '4');
					/* 当输入的值不是1,2,3,4中的任意数时将继续执行do循环体中的语句 */
				switch (CMoney)
				{
					case '1':								/* 输入值为1时执行case1 */
						system("cls");
						a=a-100;
						printf("****************************\n");
						printf("   Your Credit money is $100, Thank you!	\n");
						printf("        The balance is $%d.					\n",a);
						printf("       Press any key to return..			\n");
						printf("****************************\n");
						getch();
						break;
					case '2':								/* 输入值为2时执行case2 */
						system("cls");
						a=a-200;
						printf("****************************\n");
						printf("   Your Credit money is $200, Thank you!	\n");
						printf("        The balance is $%d.					\n",a);
						printf("       Press any key to return..			\n");
						printf("****************************\n");
						getch();
						break;
					case '3':								/* 输入值为3时执行case3 */
						system("cls");
						a=a-300;
						printf("****************************\n");
						printf("   Your Credit money is $300, Thank you!	\n");
						printf("        The balance is $%d.					\n",a);
						printf("       Press any key to return..			\n");
						printf("****************************\n");
						getch();
						break;
					case '4':								/* 输入值为4时执行case4 */
						break;
				}
				break;
				case '3':
				printf("************************************\n");
				printf("   Thank for your using!			\n");
				printf("          Goodbye!					\n");
				printf("************************************\n");
				getch();
				break;
			}
			break;
	}
	return 0;
}
