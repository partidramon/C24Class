/* 判断闰年 */

#include <stdio.h>

int main()
{
	int year;													/* 定义基本整型变量 year */
	printf("please input the year:\n");							/* 输出提示信息 */
	scanf("%d",&year);											/* 从键盘输入表示年份的整数 */
	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)	/* 判断闰年条件 */
		printf("%d is a leep year.\n", year);					/* 满足条件的输出时闰年 */
	else
		printf("%d is not a leep year.\n",year);						/* 否则输出不是闰年 */
	return 0;
}
