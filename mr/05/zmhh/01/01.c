/* 单条件分支选择语句 */

#include <stdio.h>

int main()
{
	int value;
	printf("请输入一个整数：\n");
	scanf("%d",&value);
	if (value%2==0)
	{
		printf("%d 是偶数！\n",value);
	}
	return 0;
}
