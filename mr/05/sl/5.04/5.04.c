/* 使用if……else语句得到两个数的最大值 */

#include <stdio.h>

int main()
{
	int iNumber1,iNumber2;												/* 定义变量 */

	printf("please enter two numbers:\n");								/* 信息提示 */
	scanf("%d%d",&iNumber1,&iNumber2);									/* 输入数据 */
	if (iNumber1>iNumber2)
	{
		printf("the bigger numbers is %d",iNumber1);
	}
	else
	{
		printf("the bigger numbers is %d",iNumber2);
	}
	return 0;
}
