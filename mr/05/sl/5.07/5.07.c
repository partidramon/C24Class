/* 使用else if语句郑雪修改信号灯程序 */

#include <stdio.h>

int main()
{
	int iSignal;														/* 定义变量表示信号灯的状态 */
	printf("the Red Light is 0.\nthe Green Light is 1.\nthe Yellow Light is other number.\n");	/* 输出提示信息 */
	scanf("%d",&iSignal);												/* 输入 iSignal变量 */

	if (iSignal==1)														/* 当信号灯为绿色时 */
	{
		printf("the Light is Green, cars can run.\n");
	}
	else if (iSignal==0)												/* 当信号灯为红色时 */
	{
		printf("the Light is Red, cars can not run.\n");
	}
	else																/* 当信号灯为黄色时 */
	{
		printf("the Light is Yellow, cars are ready.\n");
	}

	return 0;
}
