/* 使用条件运算符计算欠款金额 */

#include <stdio.h>

int main()
{
	float fDues;										/* 定义变量表示欠款数 */
	float fAmount;										/* 表示要还的总欠款数 */
	int iOntime;										/* 表示是否按时归还 */
	char cChar;											/* 原来接收用户输入的字符 */

	printf("Enter dues amount:\n");						/* 显示信息，提示输入欠款金额 */
	scanf("%f",&fDues);									/* 用户输入 */
	printf("On Timee?(y/n)\n");							/* 显示信息，提示还款是否按时 */
	getchar();											/* 去除回车字符 */
	cChar=getchar();									/* 得到输入的字符 */
	iOntime=(cChar=='y')?1:0;							/* 使用条件运算符根据字符选择进行选择操作 */
	fAmount=iOntime?fDues:(fDues*1.1);					/* 使用条件运算符根据 iOntime 值真假进行选择操作 */
	printf("the Amount is :%.2fn",fAmount);				/* 将计算的应还的总欠款数输出 */
	return 0;
}
