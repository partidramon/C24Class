/* 使用if……else语句模拟信号灯 */

#include <stdio.h>

int main()
{
	int iSignal;												/* 定义变量表示信号灯的状态 */
	printf("the red light is 0,\nthe green light is 1,\nthe yellow light is other number\n");	/* 输出提示信息 */
	scanf("%d",&iSignal);

	if (iSignal==1)
		printf("the light is green, cars can run.\n");
	if (iSignal==0)
		printf("the light is red, cars can not run.\n");
	else
		printf("the light is yellow, cars are ready.\n");
	return 0;
}
