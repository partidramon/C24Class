/* if语句和switch语句的综合应用 */

#include <stdio.h>

int main()
{
	int iMonth=0,iDay=0;										/* 定义变量 */
	printf("enter the month youbwant to know the days\n");		/* 提示信息 */
	scanf("%d",&iMonth);										/* 输入信息 */
	switch (iMonth)												/* 检验变量 */
	{
		/* 多路开关模式switch进行检验 */
		case 1:													/* 1为月份 */
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			iDay = 31;											/* 为 iDay 赋值为31 */
			break;												/* 跳出 switch 结构 */
		case 4:
		case 6:
		case 9:
		case 11:
			iDay = 30;											/* 为 iDay 赋值为30 */
			break;												/* 跳出 switch 结构 */
		case 2:
			iDay = 28;											/* 为 iDay 赋值为28 */
			break;												/* 跳出 switch 结构 */
		default:												/* 默认情况 */
			iDay=-1;											/* 赋值为个-1 */
			break;												/* 跳出 switch 结构 */
	}

	if (iDay==-1)												/* 使用if语句判断 iDay的值 */
	{
		printf("threr is a  error with youbenter.\n");
	}
	else														/* 默认的情况 */
	{
		printf("2021.%d has %d days.\n",iMonth,iDay);
	}
	return 0;
}
