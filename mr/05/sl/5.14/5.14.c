/* 使用 switch 语句计算运输公司的计费 */

#include <stdio.h>

int main()
{
	int iDiscount;										/* 表示折扣 */
	int iSpace;											/* 表示距离 */
	int iSwitch;										/* 表示折扣的情况 */
	float fPrice,fWeight,fAllPrice;
	printf("enter the price,weight and allprice\n");
	scanf("%f%f%d",&fPrice,&fWeight,&iSpace);
	if (iSpace>3000)
	{
		iSwitch=12;										/* 折扣的情况为12 */
	}
	else
	{
		iSwitch=iSpace/150;								/* 计算折扣的情况 */
	}

	switch (iSwitch)
	{
		case 0:
			iDiscount=0;
			break;
		case 1:
			iDiscount=2;
			break;
		case 2:
		case 3:
			iDiscount=5;
			break;
		case 5:
		case 6:
		case 7:
			iDiscount=8;
			break;
		case 8:
		case 9:
		case 10:
		case 11:
			iDiscount=10;
			break;
		case 12:
			iDiscount=12;
			break;
		default:
			iDiscount=0;
			break;
	}

	fAllPrice=fPrice*fWeight*iSpace*(1-iDiscount/100.0);	/* 计算总价格 */
	printf("AllPrice is %.4f\n",fAllPrice);					/* 输出结果 */
	return 0;
}

