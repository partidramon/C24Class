#include <stdio.h>

int main()
{
	int day, x1, x2;			/* 定义day，x1，x2 3个变量为基本整型 */
	day = 9;
	x2 = 1;
	while (day > 0)
	{
		x1 = (x2 + 1) * 2;		/* 第一天的桃子数量是第二天桃子数量加一后的两倍 */
		x2 = x1;				/* 因为从后向前推天数递减 */
		day--;
	}
	printf("the total is %d\n", x1);	/* 输出桃子的总数 */
	return 0;
}
