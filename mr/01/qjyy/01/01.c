#include <stdio.h>						/* 说明头文件 */
int main()
{
	int a, b, sum;						/* 声明变量 */
	a = 123;							/* 为变量赋于初始值 */
	b = 789;							/* 为变量赋于初始值 */
	sum = a + b;						/* 求和运算 */
	printf("sum is %d\n", sum);			/* 输出结果 */
	
	return 0;
}

